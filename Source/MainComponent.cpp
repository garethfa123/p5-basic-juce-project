/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    textButton1.setButtonText ("Click me");
    addAndMakeVisible (textButton1);
    textButton1.addListener (this);
    textButton2.setButtonText ("Click me 2");
    addAndMakeVisible (textButton2);
    textButton2.addListener (this);
    addAndMakeVisible (slider1);
    slider1.addListener (this);
    addAndMakeVisible (comboBox1);
    addAndMakeVisible (textEditor1);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    DBG ("Get width: " << getWidth());
    DBG ("Get height: " << getHeight());
    textButton1.setBounds (10,10, getWidth() - 20, 40);
    textButton2.setBounds (10,100, getWidth() - 20, 40);
    slider1.setBounds (10,200, getWidth() - 20, 40);
    comboBox1.setBounds (10,300, getWidth() - 20, 40);
    textEditor1.setBounds (10,400, getWidth() - 20, 40);
}

void MainComponent::buttonClicked (Button* button)
{
    DBG ("Button Clicked\n");
    if (button ==&textButton1)
        DBG("textButton1 clicked!\n");
    else if (button == &textButton2)
        DBG("textButton2 clicked!\n");
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    DBG ("Slider Value: " << slider1.getValue());
}
